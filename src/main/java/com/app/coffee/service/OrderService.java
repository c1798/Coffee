package com.app.coffee.service;


import com.app.coffee.vo.OrderVO;

import java.util.List;

public interface OrderService {

    List<OrderVO> getOrdersByUserId(Long userId);

    int add(OrderVO orderVO);

    int updateOrderStatus(Long orderId);
}
