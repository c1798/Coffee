package com.app.coffee.service;

import com.app.coffee.vo.ShoppingCartVO;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ShoppingCartService {
    List<ShoppingCartVO> getSHoppingCartByUserId(Long userId);

    int add(Long userId, Long productId, Integer num);
}
