package com.app.coffee.service;

import com.app.coffee.vo.MallGoodsVO;

import java.util.List;

public interface GoodsService {

    List<MallGoodsVO> getGoodsList();

    MallGoodsVO getGoodsById(Long goodsId);
}
