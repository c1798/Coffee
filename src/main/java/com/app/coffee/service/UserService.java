package com.app.coffee.service;

import com.app.coffee.vo.UserVO;

public interface UserService {
    // 注册
    int register(UserVO userVO);
    // 用户登录
    UserVO login(String userName, String password);
    // 获取用户信息
    UserVO getUserInfo(Long userId);
    // 用户信息修改并返回最新的用户信息
    int updateUserInfo(UserVO userVO);

}
