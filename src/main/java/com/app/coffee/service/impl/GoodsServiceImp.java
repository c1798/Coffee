package com.app.coffee.service.impl;

import com.app.coffee.dao.AndroidGoodsMapper;
import com.app.coffee.entity.MallGoods;
import com.app.coffee.service.GoodsService;
import com.app.coffee.vo.MallGoodsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsServiceImp implements GoodsService {
    @Autowired
    AndroidGoodsMapper androidGoodsMapper;

    @Override
    public List<MallGoodsVO> getGoodsList() {
        List<MallGoods> mallGoodsList = androidGoodsMapper.findMallGoodsList();
        List<MallGoodsVO> mallGoodsVOList = new ArrayList<>();
        mallGoodsList.forEach(mallGoods ->mallGoodsVOList.
                        add(new MallGoodsVO(mallGoods.getGoodsId(),mallGoods.getGoodsName(),mallGoods.getGoodsDescribe(),mallGoods.getPrice())));
        return mallGoodsVOList;
    }

    @Override
    public MallGoodsVO getGoodsById(Long goodsId) {
        MallGoods mallGoods = androidGoodsMapper.selectGoodsByGoodsId(goodsId);
        return new MallGoodsVO(mallGoods.getGoodsId(),mallGoods.getGoodsName(),mallGoods.getGoodsDescribe(),mallGoods.getPrice());
    }
}
