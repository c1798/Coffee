package com.app.coffee.service.impl;

import com.app.coffee.dao.AndroidShoppingCartMapper;
import com.app.coffee.dao.AndroidOrderMapper;
import com.app.coffee.dao.AndroidOrderItemMapper;
import com.app.coffee.entity.Order;
import com.app.coffee.entity.OrderItem;
import com.app.coffee.service.OrderService;
import com.app.coffee.service.UserService;
import com.app.coffee.vo.OrderItemVO;
import com.app.coffee.vo.OrderVO;
import com.app.coffee.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImp implements OrderService {
    @Resource
    AndroidOrderMapper androidOrderMapper;
    @Resource
    AndroidOrderItemMapper androidOrderItemMapper;
    @Resource
    AndroidShoppingCartMapper shoppingCartMapper;
    @Resource
    UserService userService;

    @Override
    public List<OrderVO> getOrdersByUserId(Long userId) {
        List<Order> orders = androidOrderMapper.findOrderByUserId(userId);
        System.out.println(orders);
        List<OrderVO> orderVOS = new ArrayList<>();
        for(Order o:orders){
            OrderVO orderVO = new OrderVO();
            BeanUtils.copyProperties(o, orderVO);
            List<OrderItem> orderItems = androidOrderItemMapper.findOrderItemsByOrderId(o.getOrderId());
            List<OrderItemVO> orderItemVOS = new ArrayList<>();
            for(OrderItem oi:orderItems){
                OrderItemVO oio = new OrderItemVO();
                BeanUtils.copyProperties(oi, oio);
                orderItemVOS.add(oio);
            }
            orderVO.setItemVOList(orderItemVOS);
            orderVOS.add(orderVO);
        }
        System.out.println(orderVOS);
        return orderVOS;
    }

    @Override
    public int add(OrderVO orderVO) {
        //1：保存订单
        Order order = new Order();
        BeanUtils.copyProperties(orderVO, order);
        order.setIsDeleted(false);
        order.setOrderStatus(1); //设置订单状态为1
        UserVO userVO = userService.getUserInfo(order.getUserId());
        order.setUserAddress(userVO.getAddress());
        order.setUserName(userVO.getUsername());
        order.setUserPhone(userVO.getPhone());
        int res = androidOrderMapper.insert(order);
        //3:保存订单项
        if(res>0){
            List<OrderItem> orderItems = new ArrayList<OrderItem>();
            for(OrderItemVO oi: orderVO.getItemVOList()){
                OrderItem orderItem = new OrderItem();
                BeanUtils.copyProperties(oi, orderItem);
                orderItem.setOrderId(order.getOrderId());
                orderItems.add(orderItem);
            }
            int res2 = androidOrderItemMapper.batchInsert(orderItems);
            if(res2>0){
                //批量删除购物车中的数据
                return shoppingCartMapper.deleteBatch(orderVO.getSCIds());
            }
        }
        return -1;
    }

    @Override
    public int updateOrderStatus(Long orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        order.setOrderStatus(4);
        return androidOrderMapper.updateById(order);
    }
}
