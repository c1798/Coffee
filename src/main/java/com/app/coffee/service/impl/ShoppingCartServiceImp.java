package com.app.coffee.service.impl;

import com.app.coffee.dao.AndroidShoppingCartMapper;
import com.app.coffee.entity.ShoppingCart;
import com.app.coffee.service.GoodsService;
import com.app.coffee.service.ShoppingCartService;
import com.app.coffee.vo.MallGoodsVO;
import com.app.coffee.vo.ShoppingCartVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShoppingCartServiceImp implements ShoppingCartService {

    @Autowired
    AndroidShoppingCartMapper androidShoppingCartMapper;

    @Autowired
    GoodsService goodsService;

    @Override
    public List<ShoppingCartVO> getSHoppingCartByUserId(Long userId) {
        List<ShoppingCart> shoppingCarts = androidShoppingCartMapper.findShoppingCartsByUserId(userId);
        System.out.println(shoppingCarts);
        List<ShoppingCartVO> shoppingCartVOS = new ArrayList<>();
        shoppingCarts.forEach(shoppingCart -> {
            MallGoodsVO mallGoods = goodsService.getGoodsById(shoppingCart.getGoodsId());
            System.out.println(mallGoods);
            shoppingCartVOS.add(new ShoppingCartVO(shoppingCart.getCartId(),
                    shoppingCart.getUserId(), shoppingCart.getGoodsId(),
                    shoppingCart.getGoodsCount(), mallGoods.getPrice(), false,
                    mallGoods.getName(), mallGoods.getDescribe()));
        });
        return shoppingCartVOS;
    }

    @Override
    public int add(Long userId, Long productId, Integer num) {
        ShoppingCart shoppingCart = new ShoppingCart(userId, productId, num);
        return androidShoppingCartMapper.insert(shoppingCart);
    }
}
