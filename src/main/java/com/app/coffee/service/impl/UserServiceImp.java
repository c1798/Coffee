package com.app.coffee.service.impl;

import com.app.coffee.dao.AndroidUserMapper;
import com.app.coffee.entity.MallUser;
import com.app.coffee.service.UserService;
import com.app.coffee.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImp implements UserService {
    @Autowired
    AndroidUserMapper androidUserMapper;
    @Override
    public int register(UserVO userVO) {
        MallUser registerUser = new MallUser();
        registerUser.setUsername(userVO.getUsername());
        registerUser.setPasswordM5(userVO.getPasswordM5());
        registerUser.setNickname(userVO.getNickname());
        registerUser.setAddress(userVO.getAddress());
        registerUser.setPhone(userVO.getPhone());
        return androidUserMapper.insert(registerUser);
    }

    @Override
    public UserVO login(String userName, String password) {
        MallUser user = androidUserMapper.selectByLoginNameAndPassword(userName, password);
        UserVO userVO = new UserVO();
        if (user!=null){
            BeanUtils.copyProperties(user, userVO);
            return userVO;
        }else {
            return null;
        }
    }

    @Override
    public UserVO getUserInfo(Long userId) {
        MallUser user = androidUserMapper.selectById(userId);
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        return userVO;
    }

    @Override
    public int updateUserInfo(UserVO userVO) {
        MallUser updateUser = new MallUser();
        BeanUtils.copyProperties(userVO, updateUser);
        return androidUserMapper.updateById(updateUser);
    }
}
