package com.app.coffee.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShoppingCartVO {
    private Long id;
    private Long userId;
    private Long productId;
    private Integer num;
    private BigDecimal productPrice;
    private Boolean isSelected;
    private String productName;
    private String productDesc;
}
