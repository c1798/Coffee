package com.app.coffee.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserVO {
    private Long userId;
    private String username;
    private String passwordM5;
    private String nickname;
    private String address;
    private String phone;

    public UserVO(String username, String passwordM5, String nickname, String address, String phone) {
        this.username = username;
        this.passwordM5 = passwordM5;
        this.nickname = nickname;
        this.address = address;
        this.phone = phone;
    }
}
