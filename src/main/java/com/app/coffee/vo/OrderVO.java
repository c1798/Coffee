package com.app.coffee.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderVO {
    private Long orderId;
    private String orderNo;
    private Long userId;
    private BigDecimal totalPrice;
    private Integer orderStatus; //订单状态
    private Date createTime;  //下单时间
    private Date updateTime;  //送达时间
    private List<OrderItemVO> itemVOList;  //订单项集合
    private List<Long> SCIds;
}
