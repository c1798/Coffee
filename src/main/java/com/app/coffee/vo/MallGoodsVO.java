package com.app.coffee.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MallGoodsVO {
    private Long id;
    private String Name;
    private String describe;
    private BigDecimal price;

    public MallGoodsVO(String name, String describe, BigDecimal price) {
        Name = name;
        this.describe = describe;
        this.price = price;
    }
}
