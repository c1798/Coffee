package com.app.coffee.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemVO {
    private Long orderId;
    private Long goodsId;
    private String goodsName;
    private BigDecimal sellingPrice;
    private Integer goodsCount;
}
