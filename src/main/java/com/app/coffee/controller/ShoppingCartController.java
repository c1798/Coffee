package com.app.coffee.controller;

import com.app.coffee.entity.Result;
import com.app.coffee.service.ShoppingCartService;
import com.app.coffee.vo.ShoppingCartVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    ShoppingCartService shoppingCartService;

    @GetMapping("/getShoppingCartByUserId")
    public Result<List<ShoppingCartVO>> getShoppingCartByUserId(Long userId){
        List<ShoppingCartVO> shoppingCartVOS = shoppingCartService.getSHoppingCartByUserId(userId);
        Result<List<ShoppingCartVO>> result = new Result<>();
        if (shoppingCartVOS!=null){
            result.setResultCode(200);
            result.setMessage("获取购物车列表成功");
            result.setData(shoppingCartVOS);
        }else {
            result.setResultCode(401);
            result.setMessage("获取购物车列表失败");
            result.setData(null);
        }
        return result;
    }

    @GetMapping("/add")
    public Result add(Long userId, Long productId, Integer num){
        int res = shoppingCartService.add(userId, productId, num);
        Result<ShoppingCartVO> result = new Result<>();
        if (res>=0){
            result.setResultCode(200);
            result.setMessage("添加购物车成功");
        }else {
            result.setResultCode(402);
            result.setMessage("添加购物车失败");
        }
        return result;
    }
}
