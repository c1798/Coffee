package com.app.coffee.controller;


import com.app.coffee.entity.Result;
import com.app.coffee.service.GoodsService;
import com.app.coffee.vo.MallGoodsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
public class GoodsController {
    @Autowired
    GoodsService goodsService;

    @GetMapping(value = "/getAllProducts")
    public Result<List<MallGoodsVO>> getAllProducts(){
        List<MallGoodsVO> mallGoodsVOList = goodsService.getGoodsList();
        Result<List<MallGoodsVO>> result = new Result<List<MallGoodsVO>>();
        if (mallGoodsVOList != null){
            result.setResultCode(200);
            result.setMessage("获取所有商品成功");
            result.setData(mallGoodsVOList);
        }else {
            result.setResultCode(301);
            result.setMessage("获取所有商品失败");
            result.setData(null);
        }
        return result;
    }

    @GetMapping("/getProductById")
    public Result<MallGoodsVO> getProductById(Long id){
        MallGoodsVO mallGoodsVO = goodsService.getGoodsById(id);
        Result<MallGoodsVO> result = new Result<MallGoodsVO>();
        if (mallGoodsVO != null){
            result.setResultCode(200);
            result.setMessage("获取商品成功");
            result.setData(mallGoodsVO);
        }else {
            result.setResultCode(301);
            result.setMessage("获取商品失败");
            result.setData(null);
        }
        return result;
    }
}
