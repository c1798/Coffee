package com.app.coffee.controller;

import com.app.coffee.entity.Result;
import com.app.coffee.service.UserService;
import com.app.coffee.util.MD5Util;
import com.app.coffee.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping( "/login")
    public Result<UserVO> login(@RequestParam("username") String username,
                        @RequestParam("password") String password){
        String passwordMD5 = MD5Util.MD5Encode(password);
        UserVO userVO = userService.login(username, passwordMD5);
        System.out.println("user"+username);
        Result<UserVO> result = new Result<UserVO>();
        if (userVO!=null){
            result.setResultCode(200);
            result.setMessage("登陆成功");
            result.setData(userVO);
        }else {
            result.setResultCode(201);
            result.setMessage("登录失败");
            result.setData(null);
        }
        return result;
    }
    @PostMapping("/register")
    public Result<UserVO> register(@RequestParam("username") String username, @RequestParam("password") String password,
                           @RequestParam("nickname") String nickname, @RequestParam("address") String address,
                           @RequestParam("phone") String phone){
        System.out.println("come into register");
        String passwordMD5 = MD5Util.MD5Encode(password);
        UserVO registerUser = new UserVO(username, passwordMD5, nickname, address, phone);
        int res = userService.register(registerUser);
        Result<UserVO> result = new Result<>();
        if (res>=0){
            result.setResultCode(200);
            result.setMessage("注册成功");
        }else{
            result.setResultCode(202);
            result.setMessage("注册失败");
        }
        return result;
    }
    @PostMapping("/update")
    public Result<UserVO> updateUserInfo(@RequestParam("userId") String userId, @RequestParam("username") String username,
                                 @RequestParam("password") String password, @RequestParam("nickname") String nickname,
                                 @RequestParam("address") String address, @RequestParam("phone") String phone){
        System.out.println(userId);
//        String passwordMD5 = MD5Util.MD5Encode(password);
        UserVO updateUser = new UserVO();
        if (!userId.isEmpty()){
            updateUser.setUserId(Long.valueOf(userId));
        }
        if (!username.isEmpty()){
            updateUser.setUsername(username);
        }
        if (!password.isEmpty()) {
            updateUser.setPasswordM5(password);
        }
        if (!nickname.isEmpty()) {
            updateUser.setNickname(nickname);
        }
        if (!address.isEmpty()) {
            updateUser.setAddress(address);
        }
        if (!phone.isEmpty()) {
            updateUser.setPhone(phone);
        }
        System.out.println(updateUser);
        int res = userService.updateUserInfo(updateUser);
        Result<UserVO> result = new Result<>();
        if (res>=0){
            UserVO userInfo = userService.getUserInfo(Long.valueOf(userId));
            result.setResultCode(200);
            result.setMessage("更新成功");
            result.setData(userInfo);
        }else {
            result.setResultCode(203);
            result.setMessage("更新失败");
            result.setData(null);
        }
        return result;
    }
}
