package com.app.coffee.controller;

import com.app.coffee.entity.Result;
import com.app.coffee.service.OrderService;
import com.app.coffee.util.JSONTool;
import com.app.coffee.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping("/addOrder")
    public Result<OrderVO> addOrder(@RequestParam("orderJson") String orderJson){
//        System.out.println("orderJson-------------------------->"+orderJson);
        OrderVO orderVO = JSONTool.getJsonTool().fromJson(orderJson, OrderVO.class);
//        System.out.println("orderVO-------------------------->"+orderVO);
        int res = orderService.add(orderVO);
        Result<OrderVO> result = new Result<>();
        if(res>=0){
            result.setResultCode(200);
            result.setMessage("添加订单成功");
        }else{
            result.setResultCode(501);
            result.setMessage("添加订单失败");
        }
        return result;
    }

    @GetMapping(value = "/getOrdersByUserId")
    public Result<List<OrderVO>> getOrdersByUserId(Long userId) {
        List<OrderVO> orderVOS = orderService.getOrdersByUserId(userId);
        Result<List<OrderVO>> result = new Result<>();
        if(orderVOS!=null){
            result.setResultCode(200);
            result.setMessage("获取用户订单列表成功");
            result.setData(orderVOS);
        }else{
            result.setResultCode(502);
            result.setMessage("获取用户订单列表失败");
            result.setData(null);
        }
        return result;
    }

    @GetMapping(value = "/updateOrderStatus")
    public Result<OrderVO> updateOrderStatus(Long orderId) {
        int res = orderService.updateOrderStatus(orderId);
        Result<OrderVO> result = new Result<>();
        if(res>=0){
            result.setResultCode(200);
            result.setMessage("更新用户订单列表成功");
        }else{
            result.setResultCode(503);
            result.setMessage("更新用户订单列表失败");
        }
        return result;
    }



}
