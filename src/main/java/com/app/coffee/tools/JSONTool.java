package com.app.coffee.tools;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONTool {
    private static JSONTool  jsonTool = null;//新建一个静态对象
    //Java设计模式的单例模式
    private JSONTool(){}//私有化构造方法
    public String toJson(Object object){//Object 转 json
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        return gson.toJson(object);
    }

    public static JSONTool getJsonTool() {//返回单一对象
       if(jsonTool==null){
           jsonTool= new JSONTool();
       }
       return jsonTool;
    }
}
