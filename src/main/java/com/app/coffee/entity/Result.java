package com.app.coffee.entity;

import lombok.Data;

@Data
public class Result<T> {
    private Integer resultCode;
    private String message;
    private T data;
}
