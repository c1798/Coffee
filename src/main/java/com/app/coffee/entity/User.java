package com.app.coffee.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private BigInteger userId;
    private String username;
    private String passwordM5;
    private String nickname;
    private String address;
    private String phone;
}
