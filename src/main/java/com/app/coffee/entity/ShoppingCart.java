package com.app.coffee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "shopping_cart")
public class ShoppingCart {
    /*
    *
    * */
    @TableId(type = IdType.AUTO)
    private Long cartId;
    private Long userId;
    private Long goodsId;
    private Integer goodsCount;
    @TableLogic
    private Boolean isDeleted;

    public ShoppingCart(Long userId, Long goodsId, Integer goodsCount) {
        this.userId = userId;
        this.goodsId = goodsId;
        this.goodsCount = goodsCount;
    }
}
