package com.app.coffee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@TableName(value = "user")
public class MallUser {
    @TableId(type = IdType.AUTO)
    private Long userId;
    private String nickname;
    private String username;
    private String passwordM5;
    private String phone;
    private String address;
}
