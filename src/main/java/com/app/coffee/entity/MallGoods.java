package com.app.coffee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName(value = "goods_info")
public class MallGoods {
    @TableId(type = IdType.AUTO)
    private Long goodsId;
    private String goodsName;
    private String goodsDescribe;
    private BigDecimal price;
}
