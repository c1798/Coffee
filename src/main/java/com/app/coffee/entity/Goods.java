package com.app.coffee.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName(value = "goods_info")
public class Goods {
    @TableId(type = IdType.AUTO)
    private BigInteger goodsId;
    private String goodsName;
    private String goodsDescribe;
    private BigDecimal price;
    @TableField(fill = FieldFill.INSERT)
    private Date creatTime;
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;
    @Version
    private Integer version;
    @TableLogic
    private Boolean deleted;
}
