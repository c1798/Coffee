package com.app.coffee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName(value = "order_item")
public class OrderItem {
    @TableId(type = IdType.AUTO)
    private Long orderItemId;
    private Long orderId;
    private Long goodsId;
    private String goodsName;
    private BigDecimal sellingPrice;
    private Integer goodsCount;
}
