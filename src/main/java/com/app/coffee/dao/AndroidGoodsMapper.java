package com.app.coffee.dao;

import com.app.coffee.entity.MallGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AndroidGoodsMapper extends BaseMapper<MallGoods> {

    List<MallGoods> findMallGoodsList();
    MallGoods selectGoodsByGoodsId(Long goodsId);
}
