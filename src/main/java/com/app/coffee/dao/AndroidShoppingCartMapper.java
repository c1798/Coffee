package com.app.coffee.dao;

import com.app.coffee.entity.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AndroidShoppingCartMapper extends BaseMapper<ShoppingCart> {
    List<ShoppingCart> findShoppingCartsByUserId(Long userId);

    int deleteBatch(List<Long> ids);
}
