package com.app.coffee.dao;

import com.app.coffee.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

public interface AndroidOrderMapper extends BaseMapper<Order> {

    List<Order> findOrderByUserId(Long userId);

    int updateByOrderId(Order order);

    Order findOrderByOrderNo(String orderNo);
}
