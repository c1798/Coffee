package com.app.coffee.dao;

import com.app.coffee.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AndroidOrderItemMapper extends BaseMapper<OrderItem> {

    List<OrderItem> findOrderItemsByOrderId(@Param("orderId") Long orderId);

    int batchInsert(@Param("orderItems") List<OrderItem> orderItems);
}
