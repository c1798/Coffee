package com.app.coffee.dao;

import com.app.coffee.entity.MallUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AndroidUserMapper extends BaseMapper<MallUser> {
    MallUser selectByLoginNameAndPassword(@Param("userName")String userName,@Param("password")String password);
}
