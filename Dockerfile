FROM openjdk:8
MAINTAINER chenxiao<1798261874@qq.com>
ADD *.jar /app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]